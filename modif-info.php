<?php
	$title = 'Modifier ses informations';
	$script_name = 'modif-info.php' ;
	
 include('header.php'); ?>
	
	<div class="container">
		<div class="row">
			<div class="service">
				<div class="col-md-6 col-md-offset-3">
					<div class="text-center">
						<h2>Modifier ses informations</h2>
						<p>
							<form action="enregistrer.php" method="POST">
							  <div class="form-group">
							    <label for="nom">Votre nom</label>
							    <input type="text" class="form-control" id="nom" placeholder="Nom">
							    <label for="prenom">Votre prénom</label>
							    <input type="text" class="form-control" id="prenom" placeholder="Prénom">
							    <label for="portable">Votre numéro de téléphone portable</label>
							    <input type="text" class="form-control" id="portable" placeholder="Téléphone portable">
							    <label for="fixe">Votre numéro de téléphone fixe</label>
							    <input type="text" class="form-control" id="fixe" placeholder="Téléphone fixe">
							    <label for="mail">Votre adresse mail</label>
							    <input type="text" class="form-control" id="mail" placeholder="Adresse mail">
							    <label for="mail">Votre adresse mail</label>
							    <input type="text" class="form-control" id="mail" placeholder="Adresse mail">
							    <label for="mail">Votre date de naissance</label>
							    <input type="text" class="form-control" id="date-naissance" placeholder="Date de naissance (au format JJ/MM/AAAA)">
							  </div>
							  <div class="form-group">
							    <label for="pin">Votre identifiant unique personnel</label>
							    <span class="help-block">Choisissez un code PIN de 6 caractères alphanumériques servant à vous identifier en cas d'urgence.</span>
							    <input type="text" class="form-control" id="pin" placeholder="Votre PIN">
							  </div>
							  <div class="form-group">
							    <label for="groupe-sanguin">Groupe sanguin</label>
							    <input type="text" class="form-control" id="groupe-sanguin" placeholder="Votre groupe sanguin">
							    <label for="groupe-sanguin">Nom de votre médecin traitant</label>
							    <input type="text" class="form-control" id="nom-medecin" placeholder="Nom de votre médecin traitant">
							    <label for="groupe-sanguin">Numéro de téléphone de votre médecin traitant</label>
							    <input type="text" class="form-control" id="telephone-medecin" placeholder="Téléphone de votre médecin traitant">
							    <label for="groupe-sanguin">Nom de la personne à contacter</label>
							    <input type="text" class="form-control" id="nom-personne" placeholder="Nom de la personne à contacter">
							    <label for="groupe-sanguin">Numéro de téléphone de votre médecin traitant</label>
							    <input type="text" class="form-control" id="telephone-personne" placeholder="Téléphone de la personne à contacter">
							    <label for="traitements">Traitements en cours</label>
							    <textarea class="form-control" name="traitements" id="traitements" placeholder="Traitements en cours"></textarea> 
							    <label for="maladies">Maladies connues</label>
							    <textarea class="form-control" name="maladies" id="maladies" placeholder="Maladies connues"></textarea> 
							    <label for="allergies">Allergies</label>
							    <textarea class="form-control" name="allergies" id="allergies" placeholder="Allergies"></textarea>
							  </div>
							  <button type="submit" class="btn btn-primary btn-lg">Enregister</button>
							</form> 
						</p>
					</div>
					<hr>
				</div>
			</div>
		</div>
	</div>

<?php include ('footer.php'); ?>