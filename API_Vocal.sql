-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Ven 04 Décembre 2015 à 01:18
-- Version du serveur :  5.5.42
-- Version de PHP :  5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `API_Vocal`
--

-- --------------------------------------------------------

--
-- Structure de la table `Alerte`
--

CREATE TABLE `Alerte` (
  `alerte_id` int(4) unsigned NOT NULL,
  `user_id` int(4) unsigned NOT NULL,
  `alerte_session_id` int(11) NOT NULL,
  `alerte_etat` int(4) NOT NULL,
  `alerte_date_creation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `Question`
--

CREATE TABLE `Question` (
  `question_id` int(4) unsigned NOT NULL,
  `question_intitule` varchar(255) COLLATE utf8_bin NOT NULL,
  `question_reponse_false_goto` int(4) unsigned NOT NULL,
  `question_reponse_true_goto` int(4) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `Reponse`
--

CREATE TABLE `Reponse` (
  `reponse_id` int(4) unsigned NOT NULL,
  `reponse_donnee` tinyint(1) NOT NULL,
  `question_id` int(4) unsigned NOT NULL,
  `alerte_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `User`
--

CREATE TABLE `User` (
  `user_id` int(4) unsigned NOT NULL,
  `nom` varchar(80) COLLATE utf8_bin NOT NULL,
  `prénom` varchar(80) COLLATE utf8_bin NOT NULL,
  `téléphone fixe` int(11) DEFAULT NULL,
  `téléphone portable` int(11) DEFAULT NULL,
  `adresse mail` varchar(255) COLLATE utf8_bin NOT NULL,
  `PIN (6 caractères)` varchar(6) COLLATE utf8_bin NOT NULL,
  `date de naissance` date NOT NULL,
  `groupe sanguin` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `medecin traitant (nom/téléphone)` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `personne à contacter en cas d'urgence (nom/téléphones)` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `traitement en cours` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `maladie connue` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `allergies` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Alerte`
--
ALTER TABLE `Alerte`
  ADD PRIMARY KEY (`alerte_id`);

--
-- Index pour la table `Question`
--
ALTER TABLE `Question`
  ADD PRIMARY KEY (`question_id`);

--
-- Index pour la table `Reponse`
--
ALTER TABLE `Reponse`
  ADD PRIMARY KEY (`reponse_id`);

--
-- Index pour la table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Alerte`
--
ALTER TABLE `Alerte`
  MODIFY `alerte_id` int(4) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Question`
--
ALTER TABLE `Question`
  MODIFY `question_id` int(4) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Reponse`
--
ALTER TABLE `Reponse`
  MODIFY `reponse_id` int(4) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `User`
--
ALTER TABLE `User`
  MODIFY `user_id` int(4) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
