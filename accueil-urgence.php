<?php
	$title = 'Urgence';
	$script_name = 'accueil-urgence.php' ;
	$raison_urgence ='Tremblement de terre à Haïti';
 include('header-urgence.php'); 
if(!isset($_GET['q']))
{
	$requete_select='SELECT * FROM Question WHERE question_id=1';
}
else
{
	$id_q=intval($_GET['q']);
	$requete_select= 'SELECT * FROM Question WHERE question_id='.$id_q;
}
$statement_select=$pdo->query($requete_select);
$question=$statement_select->fetch();
$id_reponse_true=$question['question_reponse_true_goto'];
$id_reponse_false=$question['question_reponse_false_goto'];
 ?>

	<div class="container">
		<div class="row">
			<div class="service">
				<div class="col-md-6 col-md-offset-3">
					<div class="text-center">
						<?php if($question['question_id']!=0){ ?>
						<h2><?php echo $question['question_intitule'];?></h2>
						<ul class="reponses">
							<a href="./accueil-urgence.php?qprec=<?php echo $question['question_id'];?>&r=1&q=<?php echo $id_reponse_true;?>"><li class="reponse oui">Oui</li></a>
							<a href="./accueil-urgence.php?qprec=<?php echo $question['question_id'];?>&r=0&q=<?php echo $id_reponse_false;?>"><li class="reponse non">Non</li></a>
						</li><?php }else{?>
						<h2>Questionnaire terminé</h2>
						<p>
							Merci de rentrer votre PIN pour vous identifier et envoyer toutes les informations nécessaires aux secouristes.
							<form action="envoi_secours.php" method="POST">
								<div class="form-group">
								  <label for="pin">Votre PIN</label>
								  <input type="text" class="form-control" id="pin" classe="pin" placeholder="Votre PIN">
								</div>
							</form>
						</p>
						<?php }?>
					</div>
					<hr>
				</div>
			</div>
		</div>
	</div>

<?php include ('footer-urgence.php'); ?>