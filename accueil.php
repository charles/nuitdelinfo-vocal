<?php
	$title = 'Accueil';
	$script_name = 'accueil.php' ;
	
?>
<?php include('header.php'); ?>
	
	<div class="container">
		<div class="row">
			<div class="slider">
				<div class="img-responsive">
					<ul class="bxslider">				
						<li><img src="img/02.jpg" alt=""/></li>
						<li><img src="img/03.jpg" alt=""/></li>
					</ul>
				</div>	
			</div>
		</div>
	</div>
		
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="text-center">
					<h2>Pourquoi s'inscrire?</h2>
					<p>Site activé par les services de secours. Il permet de désengorgé les centres d'appels et de mieux optimiser les interventions. C'est pourquoi inscrivez-vous pour ne pas perdre du temps. Chaque minute compte. </p>
				</div>
			</div>
		</div>
	</div>
<?php include ('footer.php'); ?>