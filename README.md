#Application SOS Urgence
____________________________

Cette application a pour but de décharger les centres d'appel du 15 et de faire passer le questionnaire habituellement demandé au 15 directement par l'intermédiaire de l'application web.
Pour une meilleure ergonomie, l'application est utilisable à la fois par une interface à cliquer et par une interface vocale.