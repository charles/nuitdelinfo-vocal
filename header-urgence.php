<?php session_start();
 include('config.php'); 
 $requete_session='SELECT * FROM Alerte WHERE alerte_session_id=\''.session_id().'\'';
 $statement_session=$pdo->query($requete_session);
 if($statement_session->rowCount()==0)
 {
 	$requete_enregistrer_session='INSERT INTO Alerte(alerte_session_id,alerte_etat) VALUES (\''.session_id().'\',0)';
 	$pdo->query($requete_enregistrer_session);
 }
 else
 {
 	if(isset($_GET['qprec']) && $_GET['qprec']!=0)
 	{
 		$requete_enregistrer_reponse='INSERT INTO Reponse(reponse_donnee,question_id,alerte_id) SELECT '.$_GET['r'].','.$_GET['qprec'].',alerte_id FROM Alerte WHERE alerte_session_id=\''.session_id().'\'';
 		$pdo->query($requete_enregistrer_reponse);
 	}
 	
 }
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Diagno6 - <?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="css/demo.css" />
	<link rel="stylesheet" type="text/css" href="css/set1.css" />
	<link href="css/overwrite.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<nav class="navbar navbar-default navbar-fixed-top navbar-fixed-top-urgence" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="accueil.php"><span class="nom-logo">Diagno6</span><!-- <img class="logo" src="logo.png" /> --></a>
			</div>
			<div class="navbar-collapse collapse">							
				<div class="menu">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation"><a href="#">Urgence en cours : <?php echo $raison_urgence;?></a></li>
					</ul>
				</div>
			</div>			
		</div>
	</nav>
